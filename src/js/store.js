
import { createStore } from 'framework7/lite';

const store = createStore({
  state: {
    nbtask : 0,
    products: []
  },
  getters: {
    products({ state }) {
      return JSON.parse(localStorage.getItem("products"))
    }
  },
  actions: {
    addProduct({ state }, product) {
      let products = JSON.parse(localStorage.getItem("products"))
      let nbtask = parseInt(localStorage.getItem('nbtask'));
      let idproduct = nbtask + 1;
      let text_idproduct = idproduct.toString()
      product.id = text_idproduct
      localStorage.setItem("products", JSON.stringify([...products, product]))
      localStorage.setItem("nbtask", nbtask + 1)
      state.products = [...products, product]
      state.nbtask = nbtask + 1
    },
    deleteProduct( { state }, product) {
      let products = JSON.parse(localStorage.getItem("products"))
      let nbtask = parseInt(localStorage.getItem('nbtask'));
      products = products.filter((element) => element.id !== product.id)
      localStorage.setItem("products", JSON.stringify(products))
      localStorage.setItem("nbtask", nbtask - 1)
      state.products = products
      state.nbtask = nbtask - 1
    },
    saveData({ state }) {
      localStorage.setItem("Data", JSON.stringify(state.products));
    },
  },
})
export default store;
