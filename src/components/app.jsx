import React, { useState, useEffect } from 'react';
import { getDevice } from 'framework7/lite-bundle';
import {
  f7,
  f7ready,
  App,
  Panel,
  Views,
  View,
  Popup,
  Page,
  Navbar,
  Toolbar,
  NavRight,
  Link,
  Block,
  BlockTitle,
  LoginScreen,
  LoginScreenTitle,
  List,
  ListItem,
  ListInput,
  ListButton,
  BlockFooter
} from 'framework7-react';

import capacitorApp from '../js/capacitor-app';
import routes from '../js/routes';
import store from '../js/store';
import { add } from 'dom7';

const MyApp = () => {
  // Login screen demo data
  const [task, setTask] = useState('');
  const [date, setDate] = useState('');
  const [repeat, setRepeat] = useState('');
  const [description, setDescription] = useState('');
  const device = getDevice();


  if (localStorage.getItem("nbtask") === null) {
    localStorage.setItem("nbtask", JSON.stringify(0))
  }
  if(localStorage.getItem("products") === null){
    localStorage.setItem("products", JSON.stringify( [] ))
  }

  //Function
  const addProduct = () => {
    store.dispatch('addProduct', {
      id: '0',
      title: task,
      date: date,
      repeat: repeat,
      description: description
    });
    f7.dialog.alert('Tâche ' + task + '<br>Programmée le: ' + date + '<br>comme ' + repeat, () => {
      f7.loginScreen.close()
    });
    setTimeout("location.reload(true);",1000);
  }


  // Framework7 Parameters
  const f7params = {
    name: 'TODOO', // App name
    theme: 'auto', // Automatic theme detection


    id: 'io.framework7.myapp', // App bundle ID
    // App store
    store: store,
    // App routes
    routes: routes,
    // Register service worker (only on production build)
    serviceWorker: process.env.NODE_ENV === 'production' ? {
      path: '/service-worker.js',
    } : {},
    // Input settings
    input: {
      scrollIntoViewOnFocus: device.capacitor,
      scrollIntoViewCentered: device.capacitor,
    },
    // Capacitor Statusbar settings
    statusbar: {
      iosOverlaysWebView: true,
      androidOverlaysWebView: false,
    },
  };
  const alertLoginData = () => {
    f7.dialog.alert('Tâche ' + task + '<br>Programmée le: ' + date + '<br>comme ' + repeat, () => {
      f7.loginScreen.close();
    });
  }
  f7ready(() => {

    // Init capacitor APIs (see capacitor-app.js)
    if (f7.device.capacitor) {
      capacitorApp.init(f7);
    }
    // Call F7 APIs here
  });

  return (
    <App {...f7params} >

      {/* Left panel with cover effect*/}
      <Panel left cover dark>
        <View>
          <Page>
            <Navbar title="Tutoriel" />
            <Block>
              <p>L'onglet de gauche est le menu principal :</p>
              <p>• Vous avez accès à quelques informations sur les auteurs de TODOO en cliquant sur "Qui sommes-nous ?"</p>
              <p>• Vous pouvez créer une nouvelle tâche en cliquant sur "Créer une tâche"</p>
              <p>• Vous pouvez accéder à ce tutoriel en cliquant sur "Ouvrir le tutoriel"</p>
              <p>----------</p>
              <p>L'onglet de droite contient la liste des tâches que vous avez programée :</p>
              <p>• Vous pouvez consulter une tâche en appuyant sur cette dernière</p>
              <p>• Vous pouvez supprimer un tâche en cliqant sur celle-ci puis "Supprimer la tâche"</p>
            </Block>
          </Page>
        </View>
      </Panel>

      {/* Views/Tabs container */}
      <Views tabs className="safe-areas">
        {/* Tabbar for switching views-tabs */}
        <Toolbar tabbar labels bottom>
          <Link tabLink="#view-home" tabLinkActive iconIos="f7:house_fill" iconAurora="f7:house_fill" iconMd="material:home" text="Home" />
          <Link tabLink="#view-catalog" iconIos="f7:square_list_fill" iconAurora="f7:square_list_fill" iconMd="material:view_list" text="Mes tâches" />
        </Toolbar>

        {/* Your main view/tab, should have "view-main" class. It also has "tabActive" prop */}
        <View id="view-home" main tab tabActive url="/" />

        {/* Catalog View */}
        <View id="view-catalog" name="catalog" tab url="/catalog/" />

      </Views>

      <LoginScreen id="my-login-screen">
        <View>
          <Page loginScreen>
            <LoginScreenTitle>Créer une tâche</LoginScreenTitle>
            <List form>
              <ListInput
                type="text"
                name="task"
                placeholder="Intitulé de la tâche"
                value={task}
                onInput={(e) => setTask(e.target.value)}
              ></ListInput>
              <ListInput
                type="textarea"
                name="description"
                placeholder="Description de la tâche"
                value={description}
                onInput={(e) => setDescription(e.target.value)}
              ></ListInput>
              <ListInput
                type="date"
                name="date"
                placeholder="Date de l'évènement"
                value={date}
                onInput={(e) => setDate(e.target.value)}
              ></ListInput>
              <ListInput
                type="select"
                name="repeat"
                placeholder="Répéter l'évènement ?"
                value={repeat}
                onInput={(e) => setRepeat(e.target.value)}
              >
                <option>Récurrent</option>
                <option>Unique</option>
              </ListInput>
            </List>
            <List>
              <ListButton title="Créer" onClick={addProduct} />
              <ListButton title="Fermer" onClick={() => { f7.loginScreen.close() }} />
              <BlockFooter>
                Paramétrez la future tâche.<br />Cliquez sur "Créer."
              </BlockFooter>
            </List>

          </Page>
        </View>
      </LoginScreen>



    </App>
  )
}
export default MyApp;