import React from 'react';
import { Page, Navbar, Block, BlockTitle } from 'framework7-react';

const AboutPage = () => (
  <Page>
    <Navbar title="Les auteurs" backLink="Back" />
    <Block strong>
      <p>Badr Kebrit</p>
      <p>Ingénieur</p>
    </Block>
    <Block strong>
      <p>Pierre Vasseur</p>
      <p>Fonctionnaire</p>
    </Block>
  </Page>
);

export default AboutPage;
