import React from 'react';
import { Page, Navbar, BlockTitle, Block, Button, useStore } from 'framework7-react';
import store from '../js/store';
import CatalogPage from '../pages/catalog'

const ProductPage = (props) => {
  const productId = props.f7route.params.id;
  const products = useStore('products');

  var currentProduct;
  products.forEach(function (product) {
    if (product.id === productId) {
      currentProduct = product;
    }
  });

  const deleteProduct = () => {
    store.dispatch('deleteProduct', currentProduct)
    setTimeout("location.reload(true);",1000);
  }


  return (
    <Page name="product">
      <Navbar title={currentProduct.title} backLink="Back" />
      <BlockTitle>À propos de {currentProduct.title}</BlockTitle>
      <Block strong>
        {currentProduct.description}
        <br/>
        Tâche à faire le : {currentProduct.date}
        <br/>
        {currentProduct.repeat}
        <br/>
      </Block>
      <Block>
        <Button href='/catalog/' onClick={deleteProduct}>Supprimer la tâche</Button>
      </Block>
    </Page>
  );
}

export default ProductPage;
