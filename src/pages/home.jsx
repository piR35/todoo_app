import React from 'react';
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Button
} from 'framework7-react';

const HomePage = () => (
  <Page name="home">
    {/* Top Navbar */}
    <Navbar large sliding={false}>
      <NavLeft>
        <Link iconIos="f7:menu" iconAurora="f7:menu" iconMd="material:menu" panelOpen="left" />
      </NavLeft>
      <NavTitle sliding>TODOO</NavTitle>
      <NavTitleLarge>TODOO</NavTitleLarge>
    </Navbar>

    {/* Page content */}
    <Block strong>
      <p>Voici notre application permettant de programmer et de gérer vos tâches, ainsi que d'être notifié au moment voulu.</p>
      <p>On espère que vous l'apprécierez.</p>
    </Block>

    <List>
      <ListItem link="/about/" title="Qui sommes-nous ?"/>
    </List>

    <Block strong>
      <Row>
        <Col>
          <Button large fill raised loginScreenOpen="#my-login-screen" color="red">Créer une tâche</Button>
        </Col>
      </Row>
    </Block>

    <BlockTitle>Besoin d'aide ?</BlockTitle>
    <Block strong>
      <Row>
        <Col>
          <Button panelOpen="left" small round outline fill raised>Ouvrir le tutoriel</Button>
        </Col>
      </Row>
    </Block>

  </Page>
);
export default HomePage;