import React from 'react';
import { Page, Navbar, List, ListItem, Block, Button, useStore } from 'framework7-react';
import store from '../js/store'

const CatalogPage = () => {
  const products = useStore('products');

  const addProduct = () => {
    store.dispatch('addProduct', {
      id: '3',
      title: 'Faire une lessive',
      description: 'Mercredi - 14h - unique'
    });
  }

  const deleteProduct = () => {
    store.dispatch('deleteProduct', {
      id: '3',
      title: 'Faire une lessive',
      description: 'Mercredi - 14h - unique'
    });
  }

  const saveData = () => {
    store.dispatch('saveData');
    }

  const getData = () => {
    store.dispatch('getData');
  }

  return (
    <Page name="catalog">
      <Navbar title="Liste des tâches" />
      <List>
        {
        products !== null && (
        products.map((product) => (
          <ListItem
            key={product.id}
            title={product.title}
            link={`/product/${product.id}/`
          }
          />
        )))}
      </List>
    </Page>
  );
}

export default CatalogPage;
