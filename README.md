# TODOO
Todoo est une application de gestionnaire de tâche (to do list). 

## Pour lancer l'application sur votre serveur local
Ecrire sur un terminal les lignes de commandes suivantes 
```
npm install
npm start
```

## Pour lancer l'application avec Android Studio
Ecrire sur un terminal les lignes de commandes suivantes
```
npx cap open android
```